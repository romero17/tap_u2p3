
package TAP_U2P3;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;

public class Servidor extends Conexion {
    
    public Servidor(String tipo) {
        super("servidor");
    }
    
    public void iniciarServidor(){
        try {
            System.out.println("Esperando respuesta...");
            cs = ss.accept();
            System.out.println("Cliente en linea");
            salidaCliente = new DataOutputStream(cs.getOutputStream()); 
            salidaCliente.writeUTF("Peticion recibida y aceptada");
            BufferedReader entrada = new BufferedReader(new InputStreamReader(cs.getInputStream()));
            while((mensajeServidor = entrada.readLine()) != null){
                System.out.println(mensajeServidor);
            }
            System.out.println("Fin de la conexcion");
            ss.close();
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Error en servidor");
            
        }
    }
}
