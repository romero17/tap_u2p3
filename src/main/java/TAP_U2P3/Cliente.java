
package TAP_U2P3;

import java.io.DataOutputStream;

public class Cliente extends Conexion{
    
    public Cliente(String tipo) {
        super("cliente");
    }
    
    public void iniciaCliente(){
        System.out.println("Entrada del cliente al servidor");
        try {
            
            salidaServidor = new DataOutputStream(cs.getOutputStream());
            
            salidaServidor.writeUTF("Hola\n");
            salidaServidor.writeUTF("Contraseña es incorrecta\n");
            salidaServidor.writeUTF("Adios \n");
              
            cs.close();
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Error en Cliente");
        }
    }
    
}
