
package TAP_U2P3;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Conexion {
    private final int Puerto = 9876; 
    private final String Host = "LocalHost";
    protected String mensajeServidor;
    protected ServerSocket ss; 
    protected Socket cs; // socket del cliente
    protected DataOutputStream salidaServidor; 
    protected DataOutputStream salidaCliente; //datos de salida
    
    public Conexion(String tipo){
        try {
            if(tipo.equalsIgnoreCase("servidor")){
                ss = new ServerSocket(Puerto); 
                cs = new Socket(); 
            }
            else{
                cs = new Socket(Host, Puerto); 
            }
        } catch (Exception e) {
        }
    }
    
}
